<?php

namespace Tests\Feature\Feature;

use App\models\ProductGroup;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductGroupTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testsProductGroupsAreCreatedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $payload = [
            'name' => 'electronics'
        ];

        $this->json('POST', '/api/productGroups', $payload, $headers)
            ->assertStatus(201)
            ->assertJsonFragment(['name' => 'electronics']);
    }

    public function testsProductGroupsAreUpdatedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $pg = factory(ProductGroup::class)->create([
            'name' => 'electronics'
        ]);

        $payload = [
            'name' => 'electricity'
        ];

        $response = $this->json('PUT', '/api/productGroups/' . $pg->id, $payload, $headers)
            ->assertStatus(200)
            ->assertJsonFragment([
                'name' => 'electricity'
            ]);
    }

    public function testsProductGroupsAreDeletedCorrectly()
    {
        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $pg = factory(ProductGroup::class)->create([
            'name' => 'electronics'
        ]);

        $this->json('DELETE', '/api/productGroups/' . $pg->id, [], $headers)
            ->assertStatus(204);
    }
//
    public function testsProductGroupsAreListedCorrectly()
    {
        factory(ProductGroup::class)->create([
            'name' => 'electronics'
        ]);

        factory(ProductGroup::class)->create([
            'name' => 'fashion'
        ]);

        $user = factory(User::class)->create();
        $token = $user->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $response = $this->json('GET', '/api/productGroups', [], $headers)
            ->assertStatus(200)
            ->assertJsonStructure([
                '*' => ['id', 'name', 'created_at', 'updated_at'],
            ]);
    }
}
