<?php

namespace App\Http\Controllers;

use App\models\CustomerType;
use Illuminate\Http\Request;

class CustomerTypeController extends Controller
{
    public function index()
    {
        return CustomerType::all();
    }

    public function show(CustomerType $ct)
    {
        return $ct;
    }

    public function store(Request $request)
    {
        $ct = CustomerType::create($request->all());

        return response()->json($ct, 201);
    }

    public function update(Request $request, CustomerType $ct)
    {
        $ct->update($request->all());

        return response()->json($ct, 200);
    }

    public function delete(CustomerType $ct)
    {
        $ct->delete();
        
        return response()->json(null, 204);
    }
}
