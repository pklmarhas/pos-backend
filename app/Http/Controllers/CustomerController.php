<?php

namespace App\Http\Controllers;

use App\models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index()
    {
        return Customer::all();
    }

    public function show(Customer $c)
    {
        return $c;
    }

    public function store(Request $request)
    {
        $c = Customer::create($request->all());

        return response()->json($c, 201);
    }

    public function update(Request $request, Customer $c)
    {
        $c->update($request->all());

        return response()->json($c, 200);
    }

    public function delete(Customer $c)
    {
        $c->delete();

        return response()->json(null, 204);
    }
}
