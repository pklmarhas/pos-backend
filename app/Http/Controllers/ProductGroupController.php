<?php

namespace App\Http\Controllers;

use App\models\ProductGroup;
use Illuminate\Http\Request;

class ProductGroupController extends Controller
{
    public function index()
    {
        return ProductGroup::all();
    }

    public function show(ProductGroup $pg)
    {
        return $pg;
    }

    public function store(Request $request)
    {
        $pg = ProductGroup::create($request->all());

        return response()->json($pg, 201);
    }

    public function update(Request $request, ProductGroup $pg)
    {
        $pg->update($request->all());

        return response()->json($pg, 200);
    }

    public function delete(ProductGroup $pg)
    {
        $pg->delete();

        return response()->json(null, 204);
    }
}
