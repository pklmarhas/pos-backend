<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['name', 'code', 'birth_date', 'company', 'email', 'phone', 'address', 'country', 'province', 'city', 'kecamatan', 'post_code', 'description', 'end_period', 'is_active', 'customer_type_id'];
}