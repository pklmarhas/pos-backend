<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Uom extends Model
{
    protected $fillable = ['name', 'symbol', 'description'];
}
