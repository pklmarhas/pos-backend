<?php

use Illuminate\Database\Seeder;

class UomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\models\Uom::truncate();

        \App\models\Uom::create([
            'name' => 'kilograms',
            'symbol' => 'kg',
            'description' => 'mass unit of measure'
        ]);

        \App\models\Uom::create([
            'name' => 'meters',
            'symbol' => 'm',
            'description' => 'length unit of measure'
        ]);

        \App\models\Uom::create([
            'name' => 'pounds',
            'symbol' => 'lb',
            'description' => 'mass unit of measure'
        ]);
    }
}
