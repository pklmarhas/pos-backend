<?php

use Illuminate\Database\Seeder;

class BrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\models\Brand::truncate();

        $faker = \Faker\Factory::create();

        for ($i=0;$i < 5; $i++) {
            \App\models\Brand::create([
                'name' => $faker->domainName
            ]);
        }
    }
}
