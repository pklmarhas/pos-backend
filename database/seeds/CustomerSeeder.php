<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \App\models\Customer::truncate();
        \App\models\CustomerType::truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');        

        $bronze = \App\models\CustomerType::create([
            'name' => 'bronze'
        ]);

        $silver = \App\models\CustomerType::create([
            'name' => 'silver'
        ]);

        $gold = \App\models\CustomerType::create([
            'name' => 'gold'
        ]);

        $platinum = \App\models\CustomerType::create([
            'name' => 'platinum'
        ]);
        
        $diamond = \App\models\CustomerType::create([
            'name' => 'diamond'
        ]);

         \App\models\Customer::create([
            'name' => 'Yukihiro Souma',
            'code' => '1001',
            'birth_date' => Carbon::create('1983', '10', '19'),
            'company' => 'PT.Kai Elc',
            'email' => 'gary.lumino@gmail.com',
            'phone' => '089609668043',
            'address' => 'Kp.Jambatan RT01/RW01',
            'country' => 'Indonesia',
            'province' => 'Jawa Barat',
            'city' => 'Bandung',
            'kecamatan' => 'Margahayu Selatan',
            'post_code' => '40226',
            'description' => 'Kang Somay',
            'end_period' => Carbon::create('2030', '12', '01'),
            'is_active' => '1',
            'customer_type_id' => $diamond->id
         ]);

        \App\models\Customer::create([
            'name' => 'Zelvin',
            'code' => '1002',
            'birth_date' => Carbon::create('1993', '08', '17'),
            'company' => 'PT.Aman Makmur Sentosa',
            'email' => 'ZelvinXverious@gmail.com',
            'phone' => '082170987043',
            'address' => 'Jl.Ciharum',
            'country' => 'Indonesia',
            'province' => 'Jawa Barat',
            'city' => 'Bandung',
            'kecamatan' => 'Margahayu Selatan',
            'post_code' => '40279',
            'description' => 'Pelanggan Baru',
            'end_period' => Carbon::create('2030', '05', '15'),
            'is_active' => '1',
            'customer_type_id' => $platinum->id
        ]);

        \App\models\Customer::create([
            'name' => 'Fiko',
            'code' => '1003',
            'birth_date' => Carbon::create('1999', '06', '21'),
            'company' => 'PT.Usaha Sendiri',
            'email' => 'fikojuliana@gmail.com',
            'phone' => '089587096098',
            'address' => 'Katapang',
            'country' => 'Indonesia',
            'province' => 'Jawa Barat',
            'city' => 'Bandung',
            'kecamatan' => 'Katapang Kulon',
            'post_code' => '40086',
            'description' => 'Pelanggan Baru',
            'end_period' => Carbon::create('2030', '10', '16'),
            'is_active' => '1',
            'customer_type_id' => $silver->id
        ]);
    }
}