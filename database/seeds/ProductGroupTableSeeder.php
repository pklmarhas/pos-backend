<?php

use Illuminate\Database\Seeder;

class ProductGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\models\ProductGroup::truncate();

        $faker = \Faker\Factory::create();

        for ($i=0;$i < 5; $i++) {
            \App\models\ProductGroup::create([
                'name' => $faker->streetName
            ]);
        }
    }
}
