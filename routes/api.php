<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('brands', 'BrandController@index');
    Route::get('brands/{brand}', 'BrandController@show');
    Route::post('brands', 'BrandController@store');
    Route::put('brands/{brand}', 'BrandController@update');
    Route::delete('brands/{brand}', 'BrandController@delete');

    Route::get('productGroups', 'ProductGroupController@index');
    Route::get('productGroups/{pg}', 'ProductGroupController@show');
    Route::post('productGroups', 'ProductGroupController@store');
    Route::put('productGroups/{pg}', 'ProductGroupController@update');
    Route::delete('productGroups/{pg}', 'ProductGroupController@delete');

    Route::get('customerType', 'CustomerTypeController@index');
    Route::get('customerType/{ct}', 'CustomerTypeController@show');
    Route::post('customerType', 'CustomerTypeController@store');
    Route::put('customerType/{ct}', 'CustomerTypeController@update');
    Route::delete('customerType/{ct}', 'CustomerTypeController@delete');

    Route::get('customer', 'CustomerController@index');
    Route::get('customer/{c}', 'CustomerController@show');
    Route::post('customer', 'CustomerController@store');
    Route::put('customer/{c}', 'CustomerController@update');
    Route::delete('customer/{c}', 'CustomerController@delete');

    Route::get('uoms', 'UomController@index');
    Route::get('uoms/{uom}', 'UomController@show');
    Route::post('uoms', 'UomController@store');
    Route::put('uoms/{uom}',  'UomController@update');
    Route::delete('uoms/{uom}', 'UomController@delete');
});
